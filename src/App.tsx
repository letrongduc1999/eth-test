import React from 'react';
import { ChakraProvider, useDisclosure  } from "@chakra-ui/react";
import Layout from './Layout';
import ConnectButton from './ConnectButton';
import "@fontsource/inter";
import AccountModal from './AccountModal';
import theme from './theme';

function App() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <ChakraProvider theme={theme}>
      <Layout>
        <ConnectButton handleOpenModal={onOpen} />
        <AccountModal isOpen={isOpen} onClose={onClose} />
      </Layout>
    </ChakraProvider>
  );
}

export default App;
